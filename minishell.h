/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   minishell.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ftriquet <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/21 23:52:34 by ftriquet          #+#    #+#             */
/*   Updated: 2015/12/29 18:14:31 by ftriquet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MINISHELL_H
# define MINISHELL_H
# include <unistd.h>

typedef struct		s_path
{
	char			*path;
	struct s_path	*next;
}					t_path;

typedef struct		s_env
{
	char			*key;
	char			*val;
	struct s_env	*next;
}					t_env;

typedef struct		s_shell
{
	t_env			*env;
	t_env			*env_tmp;
	char			**av;
	int				ac;
	char			*pwd;
}					t_shell;

pid_t				g_pid;

t_env				*newvar(char *key, char *val);
int					set_env(t_env **env, char *key, char *val);
int					unset_env(t_env **env, char *key);
void				del_env(t_env **env);
int					print_env(t_env *env);
char				**env_tab(t_env *env);
t_env				*tab_env(char **tab);
char				*get_env(t_env *env, char *key, int dup);
int					set_env2(t_env **env, char *keyval);
t_shell				*newshell(char **env);
int					exec_cd(t_shell *shell);
void				exec_set(t_shell *shell);
void				exec_unset(t_shell *shell);
int					exec_exit(t_shell **shell);
int					exec_builtin(t_shell **shell);
int					set_av(t_shell *shell, char **av);
int					switch_pwd(t_shell *shell);
int					is_dir(t_shell *sh, char *s);
int					move_home(t_shell *sh);
char				*real_path(t_shell *sh, char *filename);
int					cat_until(char *dst, char *src, char stop);
char				*newpath(t_shell *sh, char *path);
int					parse_cd(t_shell *sh);
int					parse_cd2(t_shell *sh);
int					exec_cd(t_shell *sh);
int					move_dir(t_shell *sh, int p_opt);
int					is_exec(char *cmd);
char				*find_path(t_shell *sh, char *cmd);
int					launch_cmd(t_shell *shell, int ind);
char				**quote_args(char *line);
int					count_wrds(char *input, char **tab, int fill);
int					exec_env(t_shell *sh);
int					parse_env2(t_shell *sh, int ind);
int					parse_env(t_shell *sh);
int					env_error(char *message);
t_env				*dup_env(t_shell *sh);
int					fuck_norm(char *path, char *pwd, int *cursor, int *i);
int					is_number(char *s);
void				handle_status(t_shell *sh, int ind, int status);
void				verif_root(char *pwd, int *cursor);
void				yoloswagggg(char *input, char **tab, int fill, int t[3]);

int					change_tmp_path(t_shell *sh, int *i);
int					unset_tmp(t_shell *sh, int *i);
#endif
