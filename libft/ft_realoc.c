/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_realloc.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ftriquet <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/20 21:33:16 by ftriquet          #+#    #+#             */
/*   Updated: 2015/12/22 16:28:17 by ftriquet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft.h"

int		ft_realoc(void **mem1, size_t size1, void *mem2, size_t size2)
{
	void	*res;

	if (!(res = malloc(size1 + size2)))
		return (-1);
	memmove(res, *mem1, size1);
	memmove(res + size1, mem2, size2);
	free(*mem1);
	*mem1 = res;
	return (1);
}
