/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ftriquet <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/23 18:57:13 by ftriquet          #+#    #+#             */
/*   Updated: 2015/12/04 22:35:15 by ftriquet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int	ft_blank(int c)
{
	return (ft_isblank(c) || c == '\v' || c == '\f' ||
			c == '\n' || c == '\r');
}

int			ft_atoi(const char *str)
{
	size_t	i;
	int		sign;
	long	result;

	result = 0;
	i = 0;
	while (ft_blank(str[i]))
		i++;
	if (str[i] == '-' || str[i] == '+')
	{
		sign = -1 * (str[i] == '-') + 1 * (str[i] == '+');
		++i;
	}
	else
		sign = 1;
	while (str[i] && ft_isdigit(str[i]))
	{
		result *= 10;
		result += str[i] - '0';
		++i;
	}
	return (sign * result);
}
