/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   gnl.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ftriquet <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/08 02:47:27 by ftriquet          #+#    #+#             */
/*   Updated: 2015/12/11 20:30:42 by ftriquet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft.h"
#include "get_next_line.h"
#include <unistd.h>

void		ft_realloc(char **line, char *tmp, size_t size)
{
	char	*new;
	size_t	len;

	if (*line)
	{
		len = ft_strlen(*line) + size + 1;
		new = malloc(sizeof(*new) * (len));
		if (new)
		{
			ft_strcpy(new, *line);
			ft_strncat(new, tmp, size);
			new[len] = '\0';
			*line = new;
		}
	}
	else
	{
		*line = malloc(sizeof(**line) * (size + 1));
		if (*line)
		{
			ft_strncpy(*line, tmp, size);
			(*line)[size] = '\0';
		}
	}
}

t_buffline	*new_buff(int const fd, t_buffline *next)
{
	t_buffline	*new;

	new = malloc(sizeof(*new));
	if (new)
	{
		new->tmp[BUFF_SIZE] = '\0';
		ft_bzero(new->tmp, BUFF_SIZE);
		new->cur = 0;
		if (next == NULL)
			new->next = new;
		else
			new->next = next;
		new->fd = fd;
	}
	return (new);
}

void		get_buffer(int const fd, t_buffline **first)
{
	t_buffline	*it;

	it = *first;
	if (*first == NULL)
	{
		*first = new_buff(fd, NULL);
		return ;
	}
	if (it->fd == fd)
		return ;
	while (it->next != *first && (it = it->next))
	{
		if (it->fd == fd)
		{
			*first = it;
			return ;
		}
	}
	if (it->fd != fd)
	{
		it->next = new_buff(fd, it->next);
		*first = it->next;
		return ;
	}
	*first = it;
}

int			get_next_line(int const fd, char **line)
{
	static t_buffline	*b = NULL;
	int					backslash;

	if (fd < 0 || !line || BUFF_SIZE < 0)
		return (-1);
	get_buffer(fd, &b);
	*line = NULL;
	if ((backslash = ft_get_index(b->tmp + b->cur, '\n')) != -1)
	{
		*line = ft_strsub(b->tmp, b->cur, backslash);
		b->cur += backslash + 1;
		return (1);
	}
	else
		*line = ft_strdup(b->tmp + b->cur);
	ft_bzero(b->tmp, BUFF_SIZE);
	b->cur = 0;
	return (gnl(fd, line, b));
}

int			gnl(int const fd, char **line, t_buffline *b)
{
	int		rd;
	char	*tmp;
	int		backslash;
	int		sav;

	sav = 0;
	while ((rd = read(fd, b->tmp, BUFF_SIZE)) > 0)
	{
		if ((backslash = ft_get_index(b->tmp + b->cur, '\n')) != -1)
		{
			tmp = ft_strsub(b->tmp, b->cur, backslash);
			ft_realloc(line, tmp, backslash);
			free(tmp);
			b->cur += backslash + 1;
			return (1);
		}
		ft_realloc(line, b->tmp, rd);
		ft_bzero(b->tmp, rd);
		sav = rd;
	}
	if (rd < 0)
		return (-1);
	return (1 && (sav != 0));
}
