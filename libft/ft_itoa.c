/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ftriquet <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/25 12:23:39 by ftriquet          #+#    #+#             */
/*   Updated: 2015/11/27 15:13:49 by ftriquet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

static unsigned int	ft_len(int n)
{
	unsigned int	len;

	len = 0;
	if (n <= 0)
		len++;
	while (n)
	{
		n /= 10;
		len++;
	}
	return (len);
}

static void			ft_itoa2(char *str, int n)
{
	*str = '0' + n % 10;
	if (n > 9)
		ft_itoa2(str - 1, n / 10);
}

char				*ft_itoa(int n)
{
	unsigned int	len;
	char			*res;

	len = ft_len(n);
	res = (char*)malloc(sizeof(*res) * (len + 1));
	if (res)
	{
		if (n < 0)
		{
			n = -n;
			res[0] = '-';
		}
		if (n == -2147483648)
		{
			res[len - 1] = '8';
			ft_itoa2(res + len - 2, 214748364);
		}
		else
			ft_itoa2(res + len - 1, n);
		res[len] = '\0';
	}
	return (res);
}
