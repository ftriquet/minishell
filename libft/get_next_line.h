/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.h                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ftriquet <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/08 01:14:11 by ftriquet          #+#    #+#             */
/*   Updated: 2015/12/11 13:25:00 by ftriquet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef GET_NEXT_LINE_H
# define GET_NEXT_LINE_H
# define BUFF_SIZE 100
# include <string.h>

typedef struct			s_buffline
{
	size_t				cur;
	char				tmp[BUFF_SIZE + 1];
	int					fd;
	struct s_buffline	*next;
}						t_buffline;
int						get_next_line(int const fd, char **line);
int						gnl(int const fd, char **line, t_buffline *b);
void					get_buffer(int const fd, t_buffline **first);
t_buffline				*new_buff(int const fd, t_buffline *next);
void					ft_realloc(char **line, char *tmp, size_t size);

#endif
