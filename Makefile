# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: ftriquet <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2015/12/29 16:09:28 by ftriquet          #+#    #+#              #
#    Updated: 2015/12/29 18:16:02 by ftriquet         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = minishell

SRC_NAME = builtins.c \
builtins2.c \
builtins3.c \
cd.c \
cd2.c \
cd3.c \
env.c \
env2.c \
main.c \
parsing.c \
path.c \
shell.c

SRCS = $(addprefix ./src/, $(SRC_NAME))
OBJ_NAME = $(SRC_NAME:.c=.o)

OBJS = $(addprefix ./obj/, $(OBJ_NAME))

CFLAGS = -Wall -Wextra -Werror

INC =  -I ./libft -I .

LFLAGS = -lft -L libft

LIB = ./libft/libft.a

all: $(NAME)

$(NAME): $(LIB) $(OBJS)
	gcc $(CFLAGS) $(OBJS) $(LFLAGS) -o $(NAME)

./obj/%.o: ./src/%.c
	mkdir -p obj
	gcc -c $(CFLAGS) $(INC) $< -o $@

$(LIB):
	make -C libft

clean:
	make clean -C libft
	rm -rf ./obj

fclean: clean
	make fclean -C libft
	rm -f $(NAME)

re: fclean all
