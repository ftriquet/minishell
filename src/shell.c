/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   shell.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ftriquet <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/22 15:56:25 by ftriquet          #+#    #+#             */
/*   Updated: 2015/12/29 18:11:12 by ftriquet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"
#include "libft.h"
#include <stdlib.h>
#include <unistd.h>

t_shell	*newshell(char **env)
{
	t_shell	*new;

	if (!(new = malloc(sizeof(*new))))
		return (NULL);
	new->env = tab_env(env);
	new->av = NULL;
	new->ac = 0;
	new->pwd = getcwd(NULL, 1000);
	new->env_tmp = NULL;
	return (new);
}

int		set_av(t_shell *shell, char **av)
{
	int		i;
	char	*tmp;
	char	*home;

	i = 0;
	if (shell->av)
		ft_freetab(shell->av);
	shell->av = av;
	while (av[i])
	{
		if (av[i][0] == '~')
		{
			if ((home = get_env(shell->env, "HOME", 0)) == NULL)
			{
				ft_putendl("minishell: HOME environnement variable is not set");
				return (-1);
			}
			tmp = ft_strjoin(home, av[i] + 1);
			free(av[i]);
			av[i] = tmp;
		}
		i++;
	}
	shell->ac = i;
	return (1);
}
