/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   env.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ftriquet <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/22 00:01:10 by ftriquet          #+#    #+#             */
/*   Updated: 2015/12/29 18:12:55 by ftriquet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"
#include <stdlib.h>
#include "libft.h"

t_env	*newvar(char *key, char *val)
{
	t_env	*new;

	if (!(new = (t_env*)malloc(sizeof(*new))))
		return (NULL);
	new->key = ft_strdup(key);
	new->val = ft_strdup(val);
	new->next = NULL;
	return (new);
}

int		set_env(t_env **env, char *key, char *val)
{
	t_env	*tmp;

	tmp = *env;
	if (*env == NULL)
	{
		*env = newvar(key, val);
		return (0);
	}
	while (tmp->next && ft_strcmp(tmp->key, key) != 0)
		tmp = tmp->next;
	if (ft_strcmp(tmp->key, key) == 0)
	{
		free(tmp->val);
		tmp->val = ft_strdup(val);
		return (1);
	}
	tmp->next = newvar(key, val);
	return (0);
}

int		set_env2(t_env **env, char *keyval)
{
	char	**split;

	split = ft_strsplit(keyval, '=');
	set_env(env, split[0], split[1]);
	ft_freetab(split);
	return (1);
}

int		unset_env(t_env **env, char *key)
{
	t_env	*tmp;
	t_env	*del;

	if (env == NULL || *env == NULL)
		return (-1);
	tmp = *env;
	if (ft_strcmp(tmp->key, key) == 0)
	{
		free(tmp->key);
		free(tmp->val);
		free(tmp);
		*env = tmp->next;
	}
	while (tmp->next && ft_strcmp(tmp->next->key, key) != 0)
		tmp = tmp->next;
	if (tmp->next == NULL)
		return (-1);
	del = tmp->next;
	free(del->key);
	free(del->val);
	tmp->next = del->next;
	free(del);
	return (0);
}

void	del_env(t_env **env)
{
	while (*env)
		unset_env(env, (*env)->key);
}
