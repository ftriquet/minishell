/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cd2.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ftriquet <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/29 15:52:25 by ftriquet          #+#    #+#             */
/*   Updated: 2015/12/29 18:06:39 by ftriquet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"
#include "libft.h"
#include <unistd.h>
#include <stdlib.h>
#include <sys/stat.h>

char	*real_path(t_shell *sh, char *filename)
{
	char	*res;

	if (filename[0] == '~')
	{
		if (get_env(sh->env, "HOME", 0) == NULL)
		{
			ft_putendl("minishell: HOME environnement variable is not set");
			return (NULL);
		}
		res = ft_strjoin(get_env(sh->env, "HOME", 0), filename + 1);
	}
	if (ft_strncmp(filename, "./", 2) != 0 &&
			ft_strncmp(filename, "../", 3) != 0 && filename[0] != '/')
	{
		res = ft_strjoin("./", filename);
	}
	else
		res = ft_strdup(filename);
	if (res && ft_strlen(res) > 1 && res[ft_strlen(res) - 1] == '/')
		res[ft_strlen(res) - 1] = '\0';
	return (res);
}

int		cat_until(char *dst, char *src, char stop)
{
	int		i;
	int		j;

	i = 0;
	j = 0;
	while (dst[j])
		j++;
	if (src[i])
		dst[j++] = '/';
	while (src[i] && src[i] != stop)
	{
		dst[i + j] = src[i];
		i++;
	}
	return (i + (src[i] != '\0'));
}

char	*newpath(t_shell *sh, char *path)
{
	int		cursor;
	int		i;
	char	pwd[1000];

	i = 0;
	if (path[0] == '/')
		return (ft_strdup(path));
	ft_strncpy(pwd, real_path(sh, sh->pwd), 1000);
	cursor = ft_strlen(pwd) - 1;
	while (path[i])
	{
		if (ft_strnequ("../", path + i, 3) || (ft_strnequ("..", path + i, 2) &&
					!path[i + 3]))
		{
			*(ft_strrchr(pwd, '/')) = '\0';
			while (cursor > 0 && pwd[cursor])
				pwd[cursor--] = '\0';
			cursor -= (cursor > 0);
			i += 2 + (path[i + 3] == '/');
		}
		else
			fuck_norm(path, pwd, &cursor, &i);
		verif_root(pwd, &cursor);
	}
	return (ft_strdup(pwd));
}

void	verif_root(char *pwd, int *cursor)
{
	if (pwd[0] == '\0')
	{
		ft_bzero(pwd, 1000);
		*cursor = 0;
		pwd[0] = '/';
	}
}

int		fuck_norm(char *path, char *pwd, int *cursor, int *i)
{
	if (ft_strnequ("./", path + *i, 2))
		*i += 2;
	else if ((path[*i] == '.' && path[*i + 1] == '\0') || path[*i] == '/')
		(*i)++;
	else
	{
		*i += cat_until(pwd, path + *i, '/');
		while (pwd[*cursor])
			(*cursor)++;
	}
	return (1);
}
