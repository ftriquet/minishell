/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   builtins.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ftriquet <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/22 15:31:02 by ftriquet          #+#    #+#             */
/*   Updated: 2015/12/29 18:21:25 by ftriquet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"
#include "libft.h"
#include <stdlib.h>
#include <unistd.h>
#include <sys/stat.h>
#include <limits.h>

int		exec_builtin(t_shell **shell)
{
	if (ft_strcmp((*shell)->av[0], "cd") == 0)
		return (exec_cd(*shell));
	if (ft_strcmp((*shell)->av[0], "setenv") == 0)
	{
		exec_set(*shell);
		return (1);
	}
	if (ft_strcmp((*shell)->av[0], "unsetenv") == 0)
	{
		exec_unset(*shell);
		return (1);
	}
	if (ft_strcmp((*shell)->av[0], "exit") == 0)
		return (exec_exit(shell));
	if (ft_strcmp((*shell)->av[0], "env") == 0)
		return (exec_env(*shell));
	return (-1);
}

int		env_error(char *message)
{
	ft_putstr("env: ");
	ft_putendl(message);
	ft_putstr("usage: env [-i] [-P path] [-u name]");
	ft_putendl(" [name=value ...] [program [arguments ...]]");
	ft_putendl("env --help for help");
	return (0);
}

int		parse_env(t_shell *sh)
{
	int		i;

	if (sh->ac == 1 || ((i = 0) != 0))
		return (1);
	while (++i < sh->ac)
	{
		if (ft_strequ(sh->av[1], "--help"))
			return (env_error(""));
		if (ft_strequ(sh->av[i], "-i"))
			del_env(&sh->env_tmp);
		else if (ft_strequ(sh->av[i], "-u") && unset_tmp(sh, &i) == 0)
			return (0);
		else if (ft_strnequ(sh->av[i], "-P", 2) && change_tmp_path(sh, &i) == 0)
			return (0);
		else if (sh->av[i][0] == '-')
			return (env_error(ft_strjoin("invalid option: ", sh->av[i] + 1)));
		else if (ft_strchr(sh->av[i], '='))
			return (parse_env2(sh, i));
		else
		{
			launch_cmd(sh, i);
			return (0);
		}
	}
	return (1);
}

int		change_tmp_path(t_shell *sh, int *i)
{
	if (sh->av[*i][2])
		set_env(&sh->env_tmp, "PATH", sh->av[*i] + 2);
	else if (sh->av[*i + 1])
	{
		set_env(&sh->env_tmp, "PATH", sh->av[*i + 1]);
		(*i)++;
	}
	else
		return (env_error("-P option requires argument"));
	return (1);
}

int		unset_tmp(t_shell *sh, int *i)
{
	if (sh->av[*i + 1])
	{
		unset_env(&sh->env_tmp, sh->av[*i + 1]);
		(*i)++;
	}
	else
		return (env_error("-u option requires argument"));
	return (1);
}
