/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   env2.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ftriquet <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/29 15:54:06 by ftriquet          #+#    #+#             */
/*   Updated: 2015/12/29 18:00:52 by ftriquet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"
#include <stdlib.h>
#include "libft.h"

int		print_env(t_env *env)
{
	while (env)
	{
		ft_putstr(env->key);
		ft_putstr("=");
		ft_putendl(env->val);
		env = env->next;
	}
	return (1);
}

char	**env_tab(t_env *env)
{
	char	**res;
	char	*tmp;
	t_env	*it;
	int		size;

	size = 0;
	it = env;
	if (!env)
		return (NULL);
	while (it && ++size)
		it = it->next;
	if (!(res = (char**)malloc(sizeof(*res) * (size + 1))))
		return (NULL);
	res[size] = NULL;
	size--;
	while (size >= 0)
	{
		tmp = ft_strjoin(env->key, "=");
		res[size] = ft_strjoin(tmp, env->val);
		free(tmp);
		size--;
		env = env->next;
	}
	return (res);
}

t_env	*tab_env(char **tab)
{
	t_env	*env;
	char	*tmp;

	env = NULL;
	if (tab == NULL)
		return (NULL);
	while (*tab)
	{
		tmp = ft_strsub(*tab, 0, ft_get_index(*tab, '='));
		set_env(&env, tmp, *tab + ft_get_index(*tab, '=') + 1);
		free(tmp);
		tab++;
	}
	return (env);
}

char	*get_env(t_env *env, char *key, int dup)
{
	while (env && ft_strcmp(key, env->key) != 0)
		env = env->next;
	if (!env)
		return (NULL);
	if (dup)
		return (ft_strdup(env->val));
	return (env->val);
}

t_env	*dup_env(t_shell *sh)
{
	t_env	*beg;
	t_env	*tmp;

	beg = NULL;
	tmp = sh->env;
	while (tmp)
	{
		set_env(&beg, tmp->key, tmp->val);
		tmp = tmp->next;
	}
	return (beg);
}
