/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cd3.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ftriquet <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/29 15:53:27 by ftriquet          #+#    #+#             */
/*   Updated: 2015/12/29 15:53:41 by ftriquet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"
#include "libft.h"
#include <unistd.h>
#include <stdlib.h>
#include <sys/stat.h>

int		switch_pwd(t_shell *shell)
{
	char	*tmp;

	if ((tmp = get_env(shell->env, "OLDPWD", 1)))
	{
		chdir(tmp);
		set_env(&shell->env, "OLDPWD", shell->pwd);
		set_env(&shell->env, "PWD", tmp);
		free(tmp);
		free(shell->pwd);
		shell->pwd = get_env(shell->env, "PWD", 1);
	}
	else
	{
		ft_putendl("cd: OLDPWD is not set");
		return (-1);
	}
	return (1);
}

int		is_dir(t_shell *sh, char *s)
{
	char		*tmp;
	struct stat	st;

	tmp = real_path(sh, s);
	if (!tmp)
		return (-2);
	if (stat(tmp, &st) == -1)
	{
		free(tmp);
		ft_putstr("cd: no such file or directory: ");
		ft_putendl(s);
		return (-1);
	}
	if (S_ISDIR(st.st_mode))
	{
		lstat(tmp, &st);
		free(tmp);
		if (S_ISLNK(st.st_mode))
			return (2);
		return (1);
	}
	ft_putstr("cd: not a directory: ");
	ft_putendl(s);
	free(tmp);
	return (-1);
}
