/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ftriquet <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/22 15:12:40 by ftriquet          #+#    #+#             */
/*   Updated: 2015/12/29 18:18:43 by ftriquet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"
#include "libft.h"
#include <stdlib.h>
#include "get_next_line.h"
#include <unistd.h>
#include <signal.h>

void	kill_child(int signum)
{
	if (signum == SIGINT)
		kill(g_pid, SIGINT);
}

int		launch_cmd(t_shell *shell, int ind)
{
	char	*cmd_path;
	int		status;

	if (!(cmd_path = find_path(shell, shell->av[ind])))
	{
		ft_putstr("minishell: command not found: ");
		ft_putendl(shell->av[ind]);
		return (-1);
	}
	signal(SIGINT, &kill_child);
	g_pid = fork();
	if (g_pid > 0)
	{
		wait(&status);
		handle_status(shell, ind, status);
		signal(SIGINT, SIG_DFL);
	}
	if (g_pid == 0)
	{
		execve(cmd_path, shell->av + ind, env_tab(shell->env_tmp));
	}
	del_env(&shell->env_tmp);
	return (1);
}

void	handle_status(t_shell *sh, int ind, int status)
{
	if (status == SIGSEGV)
	{
		ft_putstr(sh->av[ind]);
		ft_putendl(": Segmentation fault");
	}
	if (status == SIGABRT)
	{
		ft_putstr(sh->av[ind]);
		ft_putendl(": Abort");
	}
	if (status == SIGBUS)
	{
		ft_putstr(sh->av[ind]);
		ft_putendl(": Bus error");
	}
}

int		empty_line(char *s)
{
	while (*s)
	{
		if (!ft_isblank(*s))
			return (0);
		s++;
	}
	return (1);
}

int		main(void)
{
	extern char	**environ;
	t_shell		*shell;
	char		*input;

	input = NULL;
	shell = newshell(environ);
	while (shell)
	{
		shell->env_tmp = dup_env(shell);
		ft_putstr(shell->pwd);
		ft_putstr(" -> ");
		if (get_next_line(0, &input) != 1)
			break ;
		if (input[0] == '\0' || empty_line(input))
			continue ;
		if (set_av(shell, quote_args(input)) == -1)
			continue ;
		free(input);
		if (exec_builtin(&shell) == -1)
			launch_cmd(shell, 0);
		del_env(&shell->env_tmp);
	}
	return (0);
}
