/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   path.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ftriquet <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/22 15:32:07 by ftriquet          #+#    #+#             */
/*   Updated: 2015/12/29 18:20:21 by ftriquet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"
#include "libft.h"
#include <sys/stat.h>
#include <stdlib.h>
#include <unistd.h>

char	*find_path(t_shell *sh, char *cmd)
{
	char	*fullpath;
	char	**splitpath;
	int		i;
	char	*res;

	fullpath = newpath(sh, cmd);
	if (access(fullpath, F_OK) == 0 || ((i = 0) != 0))
		return (fullpath);
	free(fullpath);
	if (!(fullpath = get_env(sh->env, "PATH", 0)))
		return (NULL);
	splitpath = ft_strsplit(fullpath, ':');
	while (splitpath[i])
	{
		fullpath = ft_strjoin(splitpath[i++], "/");
		res = ft_strjoin(fullpath, cmd);
		free(fullpath);
		if (access(res, F_OK) == 0)
		{
			ft_freetab(splitpath);
			return (res);
		}
	}
	ft_freetab(splitpath);
	return (NULL);
}
