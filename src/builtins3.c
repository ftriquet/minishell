/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   builtins3.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ftriquet <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/29 16:05:32 by ftriquet          #+#    #+#             */
/*   Updated: 2015/12/29 18:07:39 by ftriquet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"
#include "libft.h"
#include <stdlib.h>
#include <unistd.h>
#include <sys/stat.h>
#include <limits.h>

void	exec_set(t_shell *shell)
{
	int		i;
	char	**split;

	i = 1;
	while (i < shell->ac)
	{
		if (ft_strchr(shell->av[i], '='))
		{
			split = ft_strsplit(shell->av[i], '=');
			set_env(&shell->env, split[0], split[1]);
			ft_freetab(split);
		}
		else
		{
			ft_putendl("Wrong syntax:");
			ft_putendl("usage: setenv key=value [otherkey=othervalue...]");
		}
		i++;
	}
}

void	exec_unset(t_shell *shell)
{
	int		i;

	i = 1;
	if (shell->ac == 1)
	{
		ft_putendl("Wrong syntax:");
		ft_putendl("Usage: unsetenv key [oterkey...]");
	}
	while (i < shell->ac)
	{
		unset_env(&shell->env, shell->av[i]);
		i++;
	}
}
