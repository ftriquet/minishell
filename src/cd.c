/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cd.c                                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ftriquet <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/26 14:48:37 by ftriquet          #+#    #+#             */
/*   Updated: 2015/12/29 18:06:17 by ftriquet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"
#include "libft.h"
#include <unistd.h>
#include <stdlib.h>
#include <sys/stat.h>

int		parse_cd(t_shell *sh)
{
	if (sh->ac > 3)
	{
		ft_putendl("cd: too many arguments");
		return (-1);
	}
	if (sh->ac == 1 || (sh->ac == 2 && ft_strequ(sh->av[1], "~")))
		return (1);
	if (sh->ac == 2)
	{
		if (ft_strequ(sh->av[1], "-"))
			return (3);
		if (is_dir(sh, sh->av[1]) > 0)
			return (2);
		return (-1);
	}
	if (!ft_strequ(sh->av[1], "-P") && !ft_strequ(sh->av[1], "-L"))
	{
		ft_putstr("cd: invalid option: ");
		ft_putendl(sh->av[1]);
		ft_putendl("usage: cd [-L|-P] [dir]");
		return (-1);
	}
	return (parse_cd2(sh));
}

int		parse_cd2(t_shell *sh)
{
	int		exists;

	if ((exists = is_dir(sh, sh->av[2])) <= 0)
		return (-1);
	if (ft_strequ(sh->av[1], "-P"))
		return (10);
	return (20);
}

int		exec_cd(t_shell *sh)
{
	int		i;

	i = parse_cd(sh);
	if (i == 3)
		return (switch_pwd(sh));
	else if (i == 1)
		return (move_home(sh));
	else if (i == 2 || i == 20)
		return (move_dir(sh, 0));
	else if (i < 0)
		return (0);
	return (move_dir(sh, 1));
}

int		move_dir(t_shell *sh, int p_opt)
{
	char	*tmp;
	char	*newpwd;

	tmp = real_path(sh, sh->av[1 + (sh->ac != 2)]);
	newpwd = newpath(sh, sh->av[1 + (sh->ac != 2)]);
	if (access(newpwd, F_OK) == 0 && chdir(newpwd) == 0)
	{
		free(tmp);
		set_env(&sh->env, "OLDPWD", sh->pwd);
		free(sh->pwd);
		if (!p_opt)
		{
			sh->pwd = newpwd;
			set_env(&sh->env, "PWD", sh->pwd);
			return (1);
		}
		free(newpwd);
		sh->pwd = getcwd(NULL, 1000);
		set_env(&sh->env, "PWD", sh->pwd);
		return (1);
	}
	ft_putendl("cd: permission denied");
	return (0);
}
