/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parsing.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ftriquet <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/23 04:30:48 by ftriquet          #+#    #+#             */
/*   Updated: 2015/12/29 18:09:54 by ftriquet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"
#include "libft.h"
#include <stdlib.h>

void	yoloswagggg(char *input, char **tab, int fill, int t[3])
{
	if (input[t[0]] == '"' && ++t[0])
	{
		while (input[t[0]] != '"')
			t[0]++;
		if (fill)
			tab[t[1]] = ft_strsub(input, t[2] + 1, t[0] - t[2] - 1);
	}
	else
	{
		while (input[t[0]] && input[t[0]] != ' ' && input[t[0]] != '\t')
			t[0]++;
		if (fill)
			tab[t[1]] = ft_strsub(input, t[2], t[0] - t[2]);
	}
}

int		count_wrds(char *input, char **tab, int fill)
{
	int		t[3];

	ft_bzero(t, 3 * sizeof(int));
	while (input[t[0]])
	{
		while (input[t[0]] == ' ' || input[t[0]] == '\t')
			t[0]++;
		t[2] = t[0];
		yoloswagggg(input, tab, fill, t);
		if (!input[t[0]] && ++t[1])
			break ;
		t[0]++;
		t[1]++;
	}
	return (t[1]);
}

char	**quote_args(char *line)
{
	int		size;
	char	**tab;

	size = count_wrds(line, NULL, 0);
	if (!(tab = (char**)malloc(sizeof(*tab) * (size + 1))))
		return (NULL);
	tab[size] = NULL;
	count_wrds(line, tab, 1);
	return (tab);
}
