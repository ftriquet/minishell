/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   builtins2.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ftriquet <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/29 16:04:21 by ftriquet          #+#    #+#             */
/*   Updated: 2015/12/29 16:04:54 by ftriquet         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"
#include "libft.h"
#include <stdlib.h>
#include <unistd.h>
#include <sys/stat.h>
#include <limits.h>

int		parse_env2(t_shell *sh, int ind)
{
	while (ind < sh->ac)
	{
		if (ft_strchr(sh->av[ind], '='))
			set_env2(&sh->env_tmp, sh->av[ind]);
		else
		{
			launch_cmd(sh, ind);
			return (0);
		}
		ind++;
	}
	return (1);
}

int		exec_env(t_shell *sh)
{
	if (parse_env(sh))
		print_env(sh->env_tmp);
	return (1);
}

int		is_number(char *s)
{
	if (ft_strlen(s) > 10)
		return (0);
	while (*s)
	{
		if (*s > '9' || *s < '0')
			return (0);
		s++;
	}
	return (1);
}

int		exec_exit(t_shell **shell)
{
	int		ret;

	ret = 0;
	if ((*shell)->ac > 2)
	{
		ft_putendl("minishell: exit: too many arguments");
		return (1);
	}
	if ((*shell)->ac == 0)
	{
		if (!is_number((*shell)->av[1]))
		{
			ft_putendl("minishell: exit: invalid argument");
			return (1);
		}
		ret = ft_atoi((*shell)->av[1]);
	}
	ft_freetab((*shell)->av);
	del_env(&(*shell)->env);
	free(*shell);
	*shell = NULL;
	exit(ret);
}

int		move_home(t_shell *shell)
{
	char	*tmp;

	if (!(tmp = get_env(shell->env, "HOME", 0)))
	{
		ft_putendl("minishell: cd: HOME environnement variable is not set");
		return (0);
	}
	set_env(&shell->env, "OLDPWD", shell->pwd);
	set_env(&shell->env, "PWD", tmp);
	chdir(get_env(shell->env, "HOME", 0));
	free(shell->pwd);
	shell->pwd = get_env(shell->env, "HOME", 1);
	return (1);
}
